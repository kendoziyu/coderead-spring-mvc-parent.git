package coderead.springframework.mvc;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 功能描述： 欢迎访客 Controller
 *
 * @author kendoziyu
 * @version 1.0.0
 */
public class HelloGuestController implements Controller {

    public ModelAndView handleRequest(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        // No mapping for GET /WEB-INF/page/hello.html
        // https://github.com/eclipse/jetty.project/archive/jetty-9.4.8.v20171121.zip
        // request.getRequestDispatcher(path).forward(request, response)
        // return new ModelAndView("/WEB-INF/page/hello.html");
        return new ModelAndView("/WEB-INF/page/hello.jsp");
    }
}
