package coderead.springframework.mvc;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 功能描述： 欢迎 Controller
 *
 * @author kendoziyu
 * @version 1.0.0
 */
public class WelcomeController implements Controller {

    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse httpServletResponse) throws Exception {
        // 注：request.getRequestDispatcher(url)之所以可以使用相对路劲，是因为在getRequestDispatcher(url)方法中封装了ServletContext.getRealPath()以获得相应的项目根路径，再通过字符串相加，从而可以获得一个完整的路径
        System.out.println(request.getServletContext().getRealPath("/WEB-INF/page/welcome.jsp"));
        return new ModelAndView("/WEB-INF/page/welcome.jsp");
    }
}
