package coderead.springframework.mvc;

import org.springframework.util.StringUtils;
import org.springframework.web.HttpRequestHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 欢迎 HttpRequestHandler
 *
 * @author kendoziyu
 * @since 2020/11/8 0008
 */
public class HelloLuBanHttpRequestHandler implements HttpRequestHandler {

    public void handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userName = request.getParameter("userName");
        if (StringUtils.isEmpty(userName)) {
            userName = "LuBan";
        }
        response.getWriter().write(String.format("<h1>Hello, %s</h1>", userName));
    }
}
