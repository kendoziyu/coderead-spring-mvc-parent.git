package coderead.springframework.requestmapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 最简单的示例类
 *
 * @author kendoziyu
 * @since 2020/11/22
 */
@Controller
public class HelloController {

    @RequestMapping("/sayHello")
    @ResponseBody
    public String printHello(@RequestParam(defaultValue = "guest") String name) {
        return "hello," + name;
    }
}
