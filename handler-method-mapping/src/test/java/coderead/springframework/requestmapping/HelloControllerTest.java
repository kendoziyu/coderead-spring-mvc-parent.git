package coderead.springframework.requestmapping;

import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 测试用反射调用 HelloController 中的方法
 *
 * @author kendoziyu
 * @since 2020/11/22
 */
public class HelloControllerTest {

    @Test
    public void doInvoke() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        // given
        HelloController bean = new HelloController();
        Class<HelloController> beanType = HelloController.class;
        Method method = beanType.getDeclaredMethod("printHello", String.class);
        // when
        Object returnValue = method.invoke(bean, "world");
        Assert.assertEquals("hello,world", returnValue);
    }
}
