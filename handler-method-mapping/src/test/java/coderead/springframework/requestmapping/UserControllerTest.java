package coderead.springframework.requestmapping;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.support.StaticWebApplicationContext;
import org.springframework.web.servlet.HandlerExecutionChain;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

/**
 * 用户控制器单元测试
 *
 * @author kendoziyu
 * @since 2020/11/28
 */
public class UserControllerTest {

    private RequestMappingHandlerMapping handlerMapping;
    private MockHttpServletRequest request;
    private StaticWebApplicationContext ctx;

    @Test
    public void initTest() throws Exception {
        request = new MockHttpServletRequest("GET", "/user/info");
        handlerMapping = new RequestMappingHandlerMapping();
        ctx = new StaticWebApplicationContext();
        // 在 afterPropertiesSet() 调用之前设置上下文
        handlerMapping.setApplicationContext(ctx);
        ctx.getBeanFactory().registerSingleton("userController", new UserController());
        handlerMapping.afterPropertiesSet();
        HandlerExecutionChain chain = handlerMapping.getHandler(request);
        Assert.assertNotNull(chain);
    }
}
