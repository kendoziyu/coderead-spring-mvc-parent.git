package coderead.springframework.requestmapping;

import org.junit.Before;
import org.junit.Test;
import org.springframework.web.context.support.StaticWebApplicationContext;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

/**
 * 初始化扫描注册和触发 HandlerMethod 的测试
 *
 * @author kendoziyu
 * @since 2020/11/22
 */
public class InitAndInvokeTest {

    private StaticWebApplicationContext webAppContext;
    private RequestMappingHandlerMapping handlerMapping;

    @Before
    public void setup() {
        webAppContext = new StaticWebApplicationContext();
        webAppContext.registerSingleton("helloController", HelloController.class);
        handlerMapping = new RequestMappingHandlerMapping();
        handlerMapping.setApplicationContext(webAppContext);
    }

    @Test
    public void test() {
        handlerMapping.afterPropertiesSet();
    }
}
